/**
 copyright(c)2021 lsl.ALL rights Reserved
 @author lsl
 @version 1.0
 @date
 */
let adminmenu = {
    "code": 0,
    "data": {
        "path": "/",
        "name": "/",
        "redirect": "/home",
        "component": "layout/index",
        "children": [
            {
                "path": "/home",
                "name": "home",
                "component": "home",
                "meta": {
                    "title": "首页",
                    "isLink": "",
                    "isHide": false,
                    "isKeepAlive": true,
                    "isAffix": true,
                    "isIframe": false,
                    "auth": ["admin", "test"],
                    "icon": "el-icon-s-home"
                }
            },
            {
                "path": "/wish",
                "name": "wish",
                "component": "wish",
                "meta": {
                    "title": "愿望管理",
                    "isLink": "",
                    "isHide": false,
                    "isKeepAlive": true,
                    "isAffix": false,
                    "isIframe": false,
                    "auth": ["admin", "test"],
                    "icon": "el-icon-eleme"
                }
            },
            {
                "path": "/user",
                "name": "user",
                "component": "user",
                "meta": {
                    "title": "账号管理",
                    "isLink": "",
                    "isHide": false,
                    "isKeepAlive": true,
                    "isAffix": false,
                    "isIframe": false,
                    "auth": ["admin", "test"],
                    "icon": "el-icon-user"
                }
            },
            {
                "path": "/wishing",
                "name": "layoutIfameView",
                "meta": {
                    "title": "许愿墙",
                    "isLink": "http://120.24.91.70:3003/",
                    "isHide": false,
                    "isKeepAlive": false,
                    "isAffix": false,
                    "isIframe": true,
                    "auth": ["admin","test"],
                    "icon": "el-icon-magic-stick"
                }
            },
            {
                "path": "/mysite",
                "name": "layoutIfameView",
                "meta": {
                    "title": "My Site",
                    "isLink": "http://120.24.91.70/",
                    "isHide": false,
                    "isKeepAlive": false,
                    "isAffix": false,
                    "isIframe": true,
                    "auth": ["admin","test"],
                    "icon": "el-icon-coffee-cup"
                }
            }
        ]
    }
}
module.exports = adminmenu
