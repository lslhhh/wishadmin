import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import Particles from 'vue-particles';
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '@/theme/index.scss';
import api from './utils/api'
Vue.use(Particles);
Vue.use(Element);

Vue.prototype.$api = api
Vue.config.productionTip = false;
Vue.prototype.bus = new Vue();

new Vue({
	router,
	store,
	render: (h) => h(App),
}).$mount('#app');
