const axios = require('axios')
const baseUrl = 'http://localhost:3002/' //本地地址


// import router from "vue-router";

const api = {}

const apiAxios = async (method, url, params) => {
    //项目既定fapp
    let headers = {fapp: 'book', 'Content-Type': 'application/json'}
    //读取存储在sessionStorage中的token
    if (sessionStorage.getItem('token')) {
        let sessiontoken = sessionStorage.getItem('token')
        // sessiontoken = sessiontoken.replaceAll('"',"")
        sessiontoken = sessiontoken.replace(/"/g,"")
        // sessiontoken = sessiontoken.replace('"',"")
        headers.token = sessiontoken
    }
    return await new Promise((resolve => {
        axios({
            //如果缓存里有token则所有请求都包含其
            headers: headers,
            method: method,
            url: baseUrl + url,
            //数据内容
            data:
                method === 'POST' || method==='PUT' || method==='DELETE'? params : null,
            params:
                method === 'GET' ? params : null

        }).then((res) => {
            console.log(res.data)

            resolve(res.data)
        }).catch(e => {
            console.log(e)
            resolve('error')
        })
    }))
}

//get请求
api.get = async (url, params,) => {
    return await apiAxios('GET', url, params)
}
//post请求
api.post = async (url, params) => {
    return await apiAxios('POST', url, params)
}
//post请求
api.put = async (url, params) => {
    return await apiAxios('PUT', url, params)
}
api.delete = async (url, params) => {
    return await apiAxios('DELETE', url, params)
}
module.exports = api

